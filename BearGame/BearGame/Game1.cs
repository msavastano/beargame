using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;    

namespace BearGame
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        StateOfGame gameState = StateOfGame.Menu;
        const int COUNTDOWN_START = 15;
        int countdown = COUNTDOWN_START;
        const int MOVE_ON_SCORE = 10;

        const int WINDOW_WIDTH = 800;
        const int WINDOW_HEIGHT = 600;

        const int COIN_MILLI = 3000;
        int elapsedMilli = 0;
        int elapsedMilliBad = 0;

        Bear bear;
        Sword sword;   
        
        bool countdownActive = true;
        int countMilli = 0;
        int toMilli = 0;
        const int TO_MILLI = 3000;
        int score = 0;
        
        const string SCORE_STRING_PREFIX = "Score: ";

        List<Coin> coins = new List<Coin>();

        Texture2D background;
        Rectangle bground;

        Texture2D playButton;
        Rectangle playRect;
        Rectangle playSource;

        Texture2D quitButton;
        Rectangle quitRect;
        Rectangle quitSource;

        bool clickStarted = false;
        bool buttonReleased = true;

        SpriteFont font;

        Random randX = new Random();
        Random randY = new Random();     

        // audio components
        AudioEngine audioEngine;
        WaveBank waveBank;
        SoundBank soundBank;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.PreferredBackBufferWidth = WINDOW_WIDTH;
            graphics.PreferredBackBufferHeight = WINDOW_HEIGHT;
            IsMouseVisible = true;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here


            background = Content.Load<Texture2D>("background");
            bground = new Rectangle(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);

            bear = new Bear(Content, "bearSword", WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_WIDTH / 2, (WINDOW_HEIGHT / 2));
            
            sword = new Sword(Content, "swordStrip", bear.X, bear.Y);

            font = Content.Load<SpriteFont>("SpriteFont1");
            

            playButton = Content.Load<Texture2D>("playbutton");
            playRect = new Rectangle(50, 50, playButton.Width / 2, playButton.Height);
            playSource = new Rectangle(0, 0, playButton.Width / 2, playButton.Height);

            quitButton = Content.Load<Texture2D>("quitButton");
            quitRect = new Rectangle(525, 50, quitButton.Width / 2, quitButton.Height);
            quitSource = new Rectangle(0, 0, quitButton.Width / 2, quitButton.Height);

            // load audio content
            audioEngine = new AudioEngine(@"Content\sounds.xgs");
            waveBank = new WaveBank(audioEngine, @"Content\Wave Bank.xwb");
            soundBank = new SoundBank(audioEngine, @"Content\Sound Bank.xsb");

            soundBank.PlayCue("backgroundMusic");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();
            
            MouseState mouse = Mouse.GetState();
            KeyboardState keyboard = Keyboard.GetState();

            if (gameState == StateOfGame.Menu)
            {
                if (playRect.Contains(mouse.X, mouse.Y))
                {
                    playSource.X = 200;
                }
                else
                {
                    playSource.X = 0;
                }

                if (quitRect.Contains(mouse.X, mouse.Y))
                {
                    quitSource.X = 200;
                }
                else
                {
                    quitSource.X = 0;
                }

                if (mouse.LeftButton == ButtonState.Pressed &&
                        buttonReleased)
                {
                    clickStarted = true;
                    if (playRect.Contains(mouse.X, mouse.Y) && clickStarted)
                    {
                        soundBank.PlayCue("buttonClick");
                        gameState = StateOfGame.Play;
                        
                        clickStarted = false;
                    }
                    else if (clickStarted && quitRect.Contains(mouse.X, mouse.Y))
                    {
                        soundBank.PlayCue("buttonClick");
                        this.Exit();
                    }
                }
                else if (mouse.LeftButton == ButtonState.Released)
                {
                    buttonReleased = true;                    
                }
            }

            if (gameState == StateOfGame.LevelChange)
            {
                toMilli += gameTime.ElapsedGameTime.Milliseconds;
                coins.Clear();
                bear.SourceRectangle = 128;
                if (toMilli > TO_MILLI)
                {                    
                    Reset();
                }            
            }

                if (gameState == StateOfGame.Play)
                {
                    if (score >= MOVE_ON_SCORE)
                    {
                        gameState = StateOfGame.LevelChange;
                    }

                    else if (countdown < 0)
                    {
                        countdownActive = false;
                        toMilli += gameTime.ElapsedGameTime.Milliseconds;
                        bear.SourceRectangle = 96;
                        coins.Clear();                        
                        
                        if (toMilli > TO_MILLI)
                        {                            
                            Reset();
                        }
                    }
                    else
                    {
                        countMilli += gameTime.ElapsedGameTime.Milliseconds;
                        if (countMilli > 1000)
                        {
                            countdown--;
                            countMilli = 0;
                        }

                        elapsedMilli += gameTime.ElapsedGameTime.Milliseconds;
                        if (elapsedMilli > COIN_MILLI)
                        {
                            int yinit = 400;
                            int ySep = 50;
                            for (int i = 0; i < 3; i++)
                            {
                                Coin newCoin = new Coin(Content, "coin",
                                        randX.Next(730, 777), yinit += ySep);

                                coins.Add(newCoin);
                            }
                            elapsedMilli = 0;
                        }

                        elapsedMilliBad += gameTime.ElapsedGameTime.Milliseconds;
                        if (elapsedMilliBad > 2 * COIN_MILLI)
                        {
                            
                            for (int i = 0; i < 1; i++)
                            {
                                BadCoin newBadCoin = new BadCoin(Content, "badCoin",
                                        randX.Next(730, 777), randY.Next(400, 575));

                                coins.Add(newBadCoin);
                                Console.WriteLine(coins[i].ToString());
                            }
                            elapsedMilliBad = 0;
                        }

                        bear.Update(keyboard, gameTime, sword.SwordActive);

                        sword.Update(keyboard, gameTime, bear.X, bear.Y, bear.Facing);

                        foreach (Coin c in coins)
                        {
                            c.Update();

                            // kill bears that are colliding 
                            if (c.Active && bear.CollisionRectangle.Intersects(c.CollisionRectangle))
                            {
                                c.Active = false;

                                if (c.isBad)
                                {
                                    soundBank.PlayCue("Bomb");
                                    score -= 10;
                                }
                                else
                                {
                                    soundBank.PlayCue("Hurt");
                                    score -= 1;
                                }
                                
                                Console.WriteLine("bear " + score);
                            }

                            if (c.Active && sword.SwordActive && sword.CollisionRectangle.Intersects(c.CollisionRectangle))
                            {
                                soundBank.PlayCue("Hit");
                                c.Active = false;

                                score += 1;
                                
                                Console.WriteLine("sword " + score);
                            }
                        }

                        for (int i = coins.Count - 1; i >= 0; i--)
                        {
                            if (!coins[i].Active)
                            {
                                coins.RemoveAt(i);
                            }
                        }

                        if (sword.SwordActive && sword.CollisionRectangle.Intersects(bear.CollisionRectangle))
                        {
                            soundBank.PlayCue("sword");
                        }
                    }

                }  

                base.Update(gameTime);
            
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            spriteBatch.Begin();
            if (gameState == StateOfGame.Play || gameState == StateOfGame.LevelChange)
            {
                spriteBatch.Draw(background, bground, Color.White);
                
                sword.Draw(spriteBatch);
                bear.Draw(spriteBatch);
                foreach (Coin c in coins)
                {
                    c.Draw(spriteBatch);
                    //Console.WriteLine(c.ToString());
                }

                spriteBatch.DrawString(font, SCORE_STRING_PREFIX + score + "/" + MOVE_ON_SCORE, new Vector2(50, 100), Color.PowderBlue);
                if (countdownActive)
                {
                    spriteBatch.DrawString(font, "Countdown! : " + countdown.ToString(), new Vector2(50, 50), Color.Black);
                }
                else
                {
                    spriteBatch.DrawString(font, "Time's UP!", new Vector2(WINDOW_WIDTH / 3, WINDOW_HEIGHT / 3), Color.White, 0, new Vector2(0, 0), 3, SpriteEffects.None, 0);
                }

                if (gameState == StateOfGame.LevelChange)
                {
                    spriteBatch.DrawString( font , "Winner!", new Vector2(WINDOW_WIDTH/3, WINDOW_HEIGHT/3) , Color.White , 0,new Vector2(0, 0), 3, SpriteEffects.None ,0);
                }
            }

            if (gameState == StateOfGame.Menu)
            {
                string dir1 = "Use arrows to move, up arrow to jump. Space bar swings power stick";
                string dir2 = "Hit  coins with stick, don't let 'em hit you, especially red ones!";
                string dir3 = "Get the right number before time's up and win a skateboard";
                spriteBatch.Draw(playButton, playRect, playSource, Color.White);
                spriteBatch.Draw(quitButton, quitRect, quitSource, Color.White);
                spriteBatch.DrawString(font, dir1 , new Vector2(10, 200), Color.White);
                spriteBatch.DrawString(font, dir2, new Vector2(10, 240), Color.White);
                spriteBatch.DrawString(font, dir3, new Vector2(10, 280), Color.White);
            }
            spriteBatch.End();


            base.Draw(gameTime);
        }

        private void Reset()
        {
            gameState = StateOfGame.Menu;
            countdown = COUNTDOWN_START;
            score = 0;
            countdownActive = true;
            toMilli = 0;
            bear.X = 0;
        
        }    
    }
}
